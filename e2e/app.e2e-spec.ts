import { MdAngularPage } from './app.po';

describe('md-angular App', () => {
  let page: MdAngularPage;

  beforeEach(() => {
    page = new MdAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
