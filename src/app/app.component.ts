import { 
    Component
    ,OnInit
    ,Input
    ,trigger
    ,state
    ,style
    ,transition
    ,animate
    ,keyframes
    } from '@angular/core';
import {TranslateService} from './md/translate/translate.service';
import {Observable} from "rxjs";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
               trigger('zoom', [
                 state('in', style({transform: 'scale(1)'})),
                     transition('void => *', [
                       animate(500, keyframes([
                         style({opacity: 1, transform: 'scale(0)', offset: 0}),
                         style({opacity: 1, transform: 'scale(1.2)',  offset: 0.8}),
                         style({opacity: 1, transform: 'scale(1)',     offset: 1.0})
                       ]))
                     ]),
                     transition('* => void', [
                       animate(500, keyframes([
                         style({opacity: 1, transform: 'scale(1)',     offset: 0}),
                         style({opacity: 1, transform: 'scale(1.2)', offset: 0.3}),
                         style({opacity: 0, transform: 'scale(0)',  offset: 1.0})
                       ]))
                     ])
               ])
             ]
})
export class AppComponent implements OnInit{
  title = '------------------------------------ Roboto ------------------------------------';
  textoTraduzido = "  "+ this.tranlateService.getValue('BEM_VINDOS') + "!";  
  curso = "Você!";
  meuNome = "EEEEEEEEEEEEEEEEEEEEEEEEEEEEE";
  picker;
  listaCursos = [
                       {id:1, nome:"Java", descricao: "A linguagem mais usada no mundo"}
                       ,{id:4, nome:"Angular", descricao: "Framework para facilidade manipulação do DOM"}
                       ,{id:2, nome:"Javascript", descricao: "Linguagem mais usada nos browsers"}
                       ,{id:3, nome:"Java EE", descricao: "Arquitertura web muito ultilizado por desenvolvedores java"}
                       ,{id:31, nome:"PHP", descricao: "Linguagem we muito ultilizada em blogs e sites"}
                       ,{id:22, nome:"Hibernate", descricao: "Framework para facilitar a pesistência de dados"}
                       ,{id:6, nome:"Spring", descricao: "Framework para facilitar o desenvolvimento"}
                       ,{id:8, nome:"Sql Server", descricao: "Banco de dados da Microsoft"}
                       ,{id:15, nome:"Oracle", descricao: "Considerado o melhor banco de dados da atulidade"}
                   ];
  curso2 = {id:2, nome:"Javascript", descricao: "Javascript versão 2015"};
  
  cursosSelecionados = [];
  cursoSelecionado = {id: 15, nome: "Scala",  descricao: "Para uso de tv"};
  
  constructor(private tranlateService: TranslateService) {}
  public setNome(){
    this.meuNome = "EEEEEEEEEEEEEEEEEEEEEEEEEEEEE";
  }
  
  diitou($event){
    console.log($event); 
  }
  
  selectedLang(lang){
      this.tranlateService.use(lang);
  }
  ngOnInit(){
      this.tranlateService.onLangChanged.subscribe(
         language => {
             console.log(language);
             this.textoTraduzido = "  "+ this.tranlateService.getValue('BEM_VINDOS') + "!";
          }
      );
  }
  
  detalheCurso(curso){
      console.log(curso);
  }
  
  teste(){
      this.cursoSelecionado = {id: 3, nome:"Rogerio", descricao: "Fazendo testes com tudo"};
  }
}
