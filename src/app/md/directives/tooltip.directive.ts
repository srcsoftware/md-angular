import { Directive, HostListener, HostBinding, ElementRef, Renderer2, Input} from '@angular/core';
import {Observable, TimeInterval} from 'rxjs/Rx';
@Directive({
  selector: '[md-tooltip]'
})
export class TooltipDirective {
    
  @Input("md-tooltip") tooltip:string;
  html:any;
  sleepShow: any;
  elementTooltip;
  isShow = false;
  
 
  constructor(
          private elementRef: ElementRef,
          private renderer2: Renderer2
  ){} 
  
  
  ngOnInit() {
      this.html =   '<div class="md-tooltip">' +
                          '<div class="md-tooltip-container">' +
                               this.tooltip +
                           '</div>' +
                           '<div class="md-tooltip-arrow "></div>' +
                     '</div>';
  }
  
  @HostListener("mouseover") onMouseOver(){
      this.hover();
  }
  @HostListener("mouseout") onMouseLeaver(){
      this.blur();
  }

  blur(){
        console.log("Blur tooltip");
        if(this.tooltip){
            if(this.sleepShow){clearInterval(this.sleepShow);}
            if(this.elementRef.nativeElement.querySelector('.md-tooltip')){
                this.renderer2.addClass(this.elementRef.nativeElement.querySelector('.md-tooltip'), "out-tooltip");
                if(this.sleepShow){clearInterval(this.sleepShow);}
                this.sleepShow = setTimeout(() => {
                        this.elementRef.nativeElement.querySelector('.md-tooltip').remove();
                }, 300);
            }
        }
    }
    
    hoverMove(){
       this.hover();
    }
    
    hover(){
        console.log("Hover tooltip");
        if(!this.elementRef.nativeElement.querySelector('.md-tooltip') && this.tooltip){
            if(this.sleepShow){clearInterval(this.sleepShow);}
            this.sleepShow = setTimeout(() => {
                    this.elementRef.nativeElement.innerHTML = this.elementRef.nativeElement.innerHTML + this.html;
                    this.setPositionTooltip();
                    this.renderer2.addClass(this.elementRef.nativeElement.querySelector('.md-tooltip'), "show-tooltip");
                    
            }, 300);
            
        }
    }
    setPositionTooltip(){
        
        let element = this.elementRef.nativeElement
        let htmlTooltip = this.elementRef.nativeElement.querySelector('.md-tooltip');
        let widthScreen = window.innerWidth;
        let heightScreen = window.innerHeight;
        let maxWidth = (widthScreen - (widthScreen * 0.2));
        let top = 0;
        let left = 0;
        if(htmlTooltip.offsetWidth >= maxWidth){
            this.renderer2.setStyle(this.elementRef.nativeElement.querySelector('.md-tooltip'), "width", maxWidth +  "px")
        }
        
        if(element.offsetTop > htmlTooltip.offsetHeight && ((element.offsetWidth / 2) + element.offsetLeft) >= (htmlTooltip.offsetWidth / 2) 
                && widthScreen - (element.offsetLeft + (element.offsetWidth - (htmlTooltip.offsetWidth /2))) > htmlTooltip.offsetWidth){
            this.renderer2.addClass(this.elementRef.nativeElement.querySelector('.md-tooltip'), "top");
            top = (element.offsetTop - (htmlTooltip.offsetHeight));
            left =  (element.offsetLeft - ((htmlTooltip.offsetWidth/2) - (element.offsetWidth /2)));
        }
        else if((widthScreen - (element.offsetLeft + (element.offsetWidth))) > htmlTooltip.offsetWidth){
            this.renderer2.addClass(this.elementRef.nativeElement.querySelector('.md-tooltip'), "right");
            top = element.offsetTop;
            left = element.offsetLeft + (element.offsetWidth);
            
        }else if(element.offsetLeft > htmlTooltip.offsetWidth){
            this.renderer2.addClass(this.elementRef.nativeElement.querySelector('.md-tooltip'), "left");
            top = element.offsetTop;
            left = (element.offsetLeft - (htmlTooltip.offsetWidth));
        }else{
            this.renderer2.addClass(this.elementRef.nativeElement.querySelector('.md-tooltip'), "bottom");
            top = element.offsetTop + (element.offsetHeight * 2.2);
            left = (widthScreen /2) - (htmlTooltip.offsetWidth/2);
            let arrowLeft = ((element.offsetLeft + (element.offsetWidth/2)) -  ((widthScreen - htmlTooltip.offsetWidth)/2));
            this.renderer2.setStyle(this.elementRef.nativeElement.querySelector('.md-tooltip-arrow'), "left", arrowLeft +  "px");
        }
        
        this.renderer2.setStyle(this.elementRef.nativeElement.querySelector('.md-tooltip'), "top",  top +  "px")
        this.renderer2.setStyle(this.elementRef.nativeElement.querySelector('.md-tooltip'), "left", left +  "px");
    }
    
    width
    
}
