import { Directive, HostListener, HostBinding, ElementRef, Renderer2, Input} from '@angular/core';
import {Observable, TimeInterval} from 'rxjs/Rx';
@Directive({
  selector: '[md-ripple]'
})
export class RippleDirective {
    
  @Input("md-ripple") mdRipple;
  
  element: any;
  circle: any;
  stopPromiss: any;
  raios = 0;
  offWidth = 0;
  offHeight = 0;
  pX = 0;
  pY = 0;
  color:any;
  inRipple = false;
  positionY = 0;
  positionX = 0;
 
  constructor(
          private elementRef: ElementRef,
          private renderer2: Renderer2
  ){
  } 
  
  
  
  @HostListener("mousedown", ['$event']) onMouseDown($event){
      console.log("Down ripple");
     this.getDados($event);
     if(this.offWidth >= this.offHeight){
         this.raios = this.offWidth; 
     }
     this.raios = (this.raios * 2.3);
     if(!this.inRipple){
         this.inRipple = true;
         this.setPosition();
         var containerRipple = '<div class="svg-ripple"><div class="circle-ripple"></div></div>';
         this.element.innerHTML = this.element.innerHTML + containerRipple;
         this.circle = this.element.querySelector('.circle-ripple');
         this.renderer2.setStyle(this.circle, "background-color", this.color);
         this.renderer2.setStyle(this.circle, "width", this.raios + "px");
         this.renderer2.setStyle(this.circle, "height", this.raios + "px");
         this.renderer2.setStyle(this.circle, "margin-top", this.positionY + "px");
         this.renderer2.setStyle(this.circle, "margin-left", this.positionX + "px");
         if(this.mdRipple && this.mdRipple != "event"){
             this.renderer2.setStyle(this.circle, "position", "absolute");
             this.renderer2.setStyle(this.element.querySelector('.svg-ripple'), "position", "relative");
             this.renderer2.setStyle(this.element.querySelector('.svg-ripple'), "overflow", "visible");
         }
         this.renderer2.addClass(this.circle, "scale-ripple-in");
    
     }
  }
  
  @HostListener("mouseup", ['$event']) onMouseUp($event){      
      console.log("mouseUp ripple");
      if(this.element && this.element.querySelector('.svg-ripple')){
          this.inRipple = true;
          this.setOutRipple(800);
      }
  }
  
  @HostListener("mouseout", ['$event']) onMouseOut($event){ 
      console.log("mouseOut ripple");
      if(this.element && this.element.querySelector('.svg-ripple')){
          this.inRipple = true;
          this.setOutRipple(1000);
      }
  }
  
  @HostListener("mousemove", ['$event']) onMouseMove($event){ 
      console.log("mouseMove ripple");
      if(this.elementRef.nativeElement && this.elementRef.nativeElement.querySelector('.svg-ripple')){
          this.inRipple = true;
          this.setOutRipple(1000);
      }
  }
 
  setPosition(){
      if(this.mdRipple && this.mdRipple != "event"){
          this.positionY = this.raios * -0.3;
          this.positionX = this.raios * -0.28;
            
      }else{
          this.positionY = ((this.pY) + (this.raios * -0.5));
          this.positionX = ((this.pX) + (this.raios * -0.5));          
      }
 }
 getDados($event){
     this.pX = $event.offsetX; 
     this.pY = $event.offsetY; 
     if(this.mdRipple && this.mdRipple == "event"){
         this.element = $event.target;
     }else{
         this.element = this.elementRef.nativeElement;
     }
     if(this.mdRipple && this.mdRipple != "event"){
         var element = this.element.querySelector(this.mdRipple);
         if(element){
             this.element = element;
         }
     }
     this.offHeight = this.element.offsetHeight;
     this.offWidth = this.element.offsetWidth;
     this.raios = this.offHeight;
     if(window.getComputedStyle(this.element).color){
         this.color = window.getComputedStyle(this.element).color;
     }else{
         this.color = "rgba(0, 0, 0, 1)"
     }
     
 }

  
  setOutRipple($time){
      if(this.circle){
          this.renderer2.removeClass(this.circle, "scale-ripple-in");
          this.renderer2.addClass(this.circle, "scale-ripple-out");
          setTimeout(()=>{
              if(this.elementRef.nativeElement.querySelector('.svg-ripple')){
                  this.elementRef.nativeElement.querySelector('.svg-ripple').remove();
              }
              this.inRipple = false;
          }, $time);
      }
  }
}
