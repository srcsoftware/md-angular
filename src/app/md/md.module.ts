import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {TranslatePipe} from "./translate/translate.pipe";
import {TRANSLATION_PROVIDERS} from "./translate/translations";
import {TranslateService} from "./translate/translate.service";
import {InputComponent} from "./components/input/input.component";
import { ButtonComponent } from './components/button/button.component';
import {TextAreaComponent} from './components/textarea/textarea.component';
import { RippleDirective } from './directives/ripple.directive';
import { TooltipDirective } from './directives/tooltip.directive';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { ToggleComponent } from './components/toggle/toggle.component';
import { RadioComponent } from './components/radio/radio.component';
import { SelectComponent } from './components/select/select.component';
import { ListComponent } from './components/list/list.component';
import { ListItemComponent } from './components/list/list-item/list-item.component';
import { OptionComponent } from './components/select/option/option.component';
import {UtilService} from "./services/util.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListTabComponent } from './components/list-tab/list-tab.component';
import { TabComponent } from './components/list-tab/tab/tab.component';
import { LoadCircleComponent } from './components/load-circle/load-circle.component';
import { LoadLineComponent } from './components/load-line/load-line.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  declarations: [
    TranslatePipe,
    InputComponent,
    ButtonComponent,
    TextAreaComponent,
    RippleDirective,
    TooltipDirective,
    CheckboxComponent,
    ToggleComponent,
    RadioComponent,
    SelectComponent,
    ListComponent,
    ListItemComponent,
    OptionComponent,
    ListTabComponent,
    TabComponent,
    LoadCircleComponent,
    LoadLineComponent
  ],
  exports: [
    TranslatePipe,
    InputComponent,
    ButtonComponent,
    TextAreaComponent,
    RippleDirective,
    TooltipDirective,
    CheckboxComponent,
    ToggleComponent,
    RadioComponent,
    SelectComponent,
    ListComponent,
    ListItemComponent,
    OptionComponent,
    ListTabComponent,
    TabComponent,
    LoadCircleComponent,
    LoadLineComponent
  ],
  providers:[
   TranslateService,
   TRANSLATION_PROVIDERS,
   UtilService
  ]
})
export class MdModule { }
