import { Component, OnInit, Input} from '@angular/core';
import {TranslateService} from "../../translate/translate.service";

@Component({
  selector: 'md-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor(
          private translate: TranslateService
  ) { }
  
  @Input() label = "";
  @Input() link = "";
  @Input() icon = "";
  @Input() width = "";
  @Input() badges = "";
  @Input() addClass = "";
  @Input() fontSize = "";
  @Input() type = "";
  @Input() tooltip:any;
  
  
  ngOnInit() {
      if(!this.type){this.type = "button"}
      let labelTranslate = this.translate.getValue(this.label);
      if(labelTranslate){
          this.label = labelTranslate;
      }
      let tooltipTranslate = this.translate.getValue(this.tooltip);
      if(tooltipTranslate){
          this.tooltip = tooltipTranslate;
      }
  }

}
