import { Component, OnInit, Input, Output, EventEmitter, ElementRef, Renderer2} from '@angular/core';
import { Observable } from 'rxjs';
import {TranslateService} from "../../translate/translate.service";

@Component({
  selector: 'md-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  constructor(
              private element: ElementRef,
              private render: Renderer2,
              private translate: TranslateService
          ) { 
  }
  empty = "";
  isFocused = "";
  @Input("value") _value:any = "";
  @Output() valueChange = new EventEmitter();
  @Input() addClass = "";
  @Input() width = "";
  @Input() type = "text";
  @Input() fontSize = "";
  @Input() iconBefore = "";
  @Input() iconAfter = "";
  @Input() label = "";
  @Input() placeholder = "";
  @Input() msgInput = "";
  
  
  get value(): any {
    this.setChange();
    return this._value;
  }
  set value(value:any) {
    this._value = value;
    this.valueChange.emit(this._value);
    this.setChange();
  }  

  ngOnInit() {
      if(this.label){
          this.placeholder = ""
          let labelTranslate = this.translate.getValue(this.label);
          if(labelTranslate){
              this.label = labelTranslate;
          }
       }
      if(!this.type){this.type = "text"}
  }
  
  setChange(){
    if(this._value){
      this.empty = "";
      
    }else{
      this.empty = "is-empty";
    }
  }
  focusIn(){
      this.isFocused = "is-focused";
  }
  focusOut(){
      this.isFocused = "";
  }
  
  
}
