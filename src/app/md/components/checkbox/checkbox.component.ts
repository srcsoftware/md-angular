import { Component, OnInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import {TranslateService} from "../../translate/translate.service";

@Component({
  selector: 'md-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  constructor(
              private elementRef: ElementRef,
              private translate: TranslateService
            ) {
  }
  

  @Input("value") _value:any = false;
  @Output() valueChange = new EventEmitter();
  @Input() addClass = "";
  @Input() width = "";
  @Input() fontSize = "";
  @Input() label:any;
  @Input() disabled = false;
  @Input() msgInput = "";
  @Input() tooltip:any;
  
  
  get value(): any {
    return this._value;
  }
  
  set value(value:any) {
    this._value = value;
    this.valueChange.emit(this._value);
  }  

  ngOnInit() {
      if(!this.label){
          this.label = '-';
      }else{
          let labelTranslate = this.translate.getValue(this.label);
          if(labelTranslate){
              this.label = labelTranslate;
          }
      }
      let tooltipTranslate = this.translate.getValue(this.tooltip);
      if(tooltipTranslate){
          this.tooltip = tooltipTranslate;
      }
  }
  check(){
      if(!this.disabled){
          this.value = !this.value;
      }
  }

}
