import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'md-load-line',
  templateUrl: './load-line.component.html',
  styleUrls: ['./load-line.component.scss']
})
export class LoadLineComponent implements OnInit {

  
    @Input() addClass:any;
    @Input() width = 12;
    @Input() show:any;
    
    constructor() { }

    ngOnInit() {
        
    }

}
