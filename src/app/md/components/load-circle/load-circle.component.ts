import { Component, OnInit, Input, Renderer2, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'md-load-circle',
  templateUrl: './load-circle.component.html',
  styleUrls: ['./load-circle.component.scss']
})
export class LoadCircleComponent implements OnInit {
    
  @Input() addClass:any;
  @Input() size:any;
  @Input() width:any;
  @Input() label:any;
  @Input() show:any;
  
  constructor(
          private elementRef: ElementRef,
          private renderer: Renderer2
  ) { }

  ngOnInit() {}  
}
