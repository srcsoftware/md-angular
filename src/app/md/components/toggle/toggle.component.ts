import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'md-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss']
})
export class ToggleComponent implements OnInit {

  constructor() { }

  @Input("value") _value:any = false;
  @Output() valueChange = new EventEmitter();
  @Input() addClass = "";
  @Input() width = "";
  @Input() fontSize = "";
  @Input() label = "-";
  @Input() disabled = false;
  @Input() msgInput = "";
  @Input() tooltip:any;
  
  
  get value(): any {
    return this._value;
  }
  
  set value(value:any) {
    this._value = value;
    this.valueChange.emit(this._value);
  }  

  check(){
      if(!this.disabled){
          this.value = !this.value;
      }
  }
  ngOnInit() {

  }
}
