import {
    Component, 
    OnInit, 
    Input, 
    ElementRef, 
    Renderer2,
    trigger,
    state,
    style,
    keyframes,
    transition,
    animate,
    HostListener
} from '@angular/core';
import {UtilService} from "../../services/util.service";
import {TabComponent} from "./tab/tab.component";

@Component({
  selector: 'md-list-tab',
  templateUrl: './list-tab.component.html',
  styleUrls: ['./list-tab.component.scss']
})
export class ListTabComponent implements OnInit {

  constructor(
          private elementRef: ElementRef,
          private renderer: Renderer2
  ) { }
  
  @Input() width = 12;
  @Input() height;
  @Input() addClass;
  heightBody:any;
  list:TabComponent[] = [];
  tabNow: TabComponent;
  tabKeyDown: TabComponent;
  showBtnBack = false;
  showBtnNext = false;
  marginLeftTittles = 0;
  marginLeftTittlesPx = "0px";
  listWidth = 0;
  tabsWidth = 0;
  afterLoad:any;
  btnWidthBack = 0;
  btnWidthNext = 0;
  maxSlideLeft = 0;
  maxSlideRight = 0;
  positionBtnLeft = "0px";
  positionBtnRight ="0px";
  
  ngOnInit() {
  }
  
  @HostListener('window:resize', ['$event'])  onResize(event) {
    this.setAfterLoad();
  }
  
  addTab(tab: TabComponent){
      this.list.push(tab);
  }
  
  clickTab(tab:TabComponent, index){
      this.tabKeyDown = tab;
      this.tabKeyDown.setIndex(index);
  }
  
  onMouseUp(){
      if(this.tabKeyDown){
          this.setStateTabs(this.tabKeyDown, this.tabKeyDown.getIndex());
          this.setBtnBackNext(this.tabKeyDown);
          this.setSlideBarAnimate(this.tabKeyDown.getIndex());
      }
  }
  
  setStateTabs(tab: TabComponent, index){
      this.setActive(index);
      if(this.tabNow){
          if(index > this.tabNow.getIndex()){
              this.tabNow.setState("outLeft");
              tab.setState("inRight");
          }else if(index < this.tabNow.getIndex()){
              this.tabNow.setState("outRight");
              tab.setState("inLeft");
          }
      }else{
          tab.setState("inLeft");
      }
      tab.setIndex(index);
      this.tabNow = tab;
      if(!this.height){
          this.heightBody = tab.getElementRef().nativeElement.querySelector(".md-tab-container").offsetHeight + "px";
      }
  }
  
  setActive(index){
      if(this.tabNow){
          this.renderer.removeClass(this.elementRef.nativeElement.querySelector("#tab"+ this.tabNow.getIndex()), "active");
      }
      this.renderer.addClass(this.elementRef.nativeElement.querySelector("#tab"+ index), "active");
  }
  
  setSlideBarAnimate(index){
      let tabClick = this.elementRef.nativeElement.querySelector("#tab" + index);
      let slideBar = this.elementRef.nativeElement.querySelector(".md-list-tab-bar-animate");
      this.renderer.setStyle(slideBar, "margin-left", tabClick.offsetLeft + "px");
      this.renderer.setStyle(slideBar, "width", tabClick.offsetWidth + "px");
  }
  
  getWidthBtnBack(){
      if(this.showBtnBack){
         if(this.btnWidthBack == 0){
             this.btnWidthBack = this.elementRef.nativeElement.querySelector(".md-list-tab-btn-back").offsetWidth;
         }
         return this.btnWidthBack;
      }else{
          return 0;
      }
  }
  
  getWidthBtnNext(){
      if(this.showBtnNext){
          if(this.btnWidthNext == 0){
              this.btnWidthNext = this.elementRef.nativeElement.querySelector(".md-list-tab-btn-next").offsetWidth;
          }
          return  this.btnWidthNext;
      }else{
          return 0;
      }
      
  }
  
  setBtnBackNext(tab: TabComponent){
     if(this.marginLeftTittles <= this.maxSlideLeft){
      
          this.setShowMarginBtnNext(true);
          this.setShowMarginBtnBack(false);
          this.marginLeftTittles = this.maxSlideLeft - this.getWidthBtnNext();
          this.renderer.setStyle(this.elementRef.nativeElement.querySelector(".md-list-tab-container-titles"), "margin-right", "0px");
      }
     else{
          this.setShowMarginBtnBack(true);
         if(this.maxSlideRight > this.listWidth){
             this.setShowMarginBtnNext(true);
         }
      }
    
      if(this.marginLeftTittles >= 0){
              this.setShowMarginBtnNext(false);
              this.marginLeftTittles = 0;
          if(this.tabsWidth > this.listWidth && this.marginLeftTittles >= this.maxSlideLeft){
              this.setShowMarginBtnBack(true);
          }
      } 
      
      if(tab){
          this.setPositionClicked(tab);
       }
      this.marginLeftTittlesPx =  this.marginLeftTittles +  "px";
  }
  
  setPositionClicked(tab: TabComponent){
      let tabLeft = this.elementRef.nativeElement.querySelector("#tab" + tab.getIndex()).offsetLeft;
      let tabWidth = this.elementRef.nativeElement.querySelector("#tab" + tab.getIndex()).offsetWidth;
      let position = (tabLeft + this.marginLeftTittles);
      let maxVisibled = this.listWidth - tabWidth - this.getWidthBtnNext();
      if(position >= (this.listWidth/2) && position > maxVisibled){
          this.marginLeftTittles = this.marginLeftTittles - (((position + tabWidth) + this.getWidthBtnBack() + this.getWidthBtnNext()) - this.listWidth);
      }else if(position < 0){
          this.marginLeftTittles = this.marginLeftTittles + (position * -1) + this.getWidthBtnBack();
      }
  }
  
  backTabs(){
      this.marginLeftTittles = (this.listWidth * -0.5) + this.marginLeftTittles;
      this.tabKeyDown = null;
      this.setBtnBackNext(null);
  }
  
  nextTabs(){
      this.marginLeftTittles = (this.listWidth * 0.5) + this.marginLeftTittles;
      this.tabKeyDown = null;
      this.setBtnBackNext(null);
  }
  
  setShowMarginBtnBack(isShow){
      this.showBtnBack = isShow;
      if(isShow){
          this.renderer.setStyle(this.elementRef.nativeElement.querySelector(".md-list-tab-container"), "margin-left", "1.72em");
          this.positionBtnLeft = "0px";
      }else{
          this.renderer.setStyle(this.elementRef.nativeElement.querySelector(".md-list-tab-container"), "margin-left", "0em");
          this.positionBtnLeft =  "-" + (this.btnWidthBack* 2) + "px";
      }
  }
  
  setShowMarginBtnNext(isShow){
      this.showBtnNext = isShow;
      if(isShow){
          this.renderer.setStyle(this.elementRef.nativeElement.querySelector(".md-list-tab-container"), "margin-right", "1.72em");
          this.positionBtnRight = "0px";
      }else{
          this.renderer.setStyle(this.elementRef.nativeElement.querySelector(".md-list-tab-container"), "margin-right", "0em");
          this.positionBtnRight =  "-" + (this.btnWidthNext* 2) + "px";
      }
  }
  
  getWidthListAndTabs(){
      this.listWidth = this.elementRef.nativeElement.querySelector(".md-list-tab-container-out").offsetWidth;
      this.tabsWidth = this.elementRef.nativeElement.querySelector(".md-list-tab-container-titles").offsetWidth;
      this.maxSlideLeft = this.listWidth - this.tabsWidth;
      this.maxSlideRight = this.marginLeftTittles + this.tabsWidth;
  }
  
  setAfterLoad(){
      if(this.afterLoad){
          clearInterval(this.afterLoad);
      }
      this.afterLoad =  setTimeout(() => {
          this.showBtnBack = true;
          this.showBtnNext = true;
          this.btnWidthBack = this.elementRef.nativeElement.querySelector(".md-list-tab-btn-back").offsetWidth;
          this.btnWidthNext = this.elementRef.nativeElement.querySelector(".md-list-tab-btn-next").offsetWidth;
          this.getWidthListAndTabs();
          this.setBtnBackNext(null);
          if(!this.tabNow && this.list.length > 0){
              this.setStateTabs(this.list[0], 0);
          }
          this.setSlideBarAnimate(this.tabNow.getIndex());
          if(this.height){
              this.heightBody = this.height;
          }
       }, 500);
  }
      
}
