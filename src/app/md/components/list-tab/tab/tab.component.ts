import {
    Component, 
    OnInit, 
    Input, 
    Optional, 
    Host, 
    ElementRef,
    trigger,
    state,
    transition,
    animate,
    style,
    keyframes
} from '@angular/core';
import {ListTabComponent} from "../list-tab.component";

@Component({
  selector: 'md-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
  animations: [
               trigger('sledeTab', [
                 state('init', style({opacity: '0', transform: 'translateX(-100%)'})),
                 state('outLeft', style({opacity: '0', transform: 'translateX(-100%)'})),
                 state('outRight', style({opacity: '0', transform: 'translateX(100%)'})),
                 state('inLeft', style({opacity: '1', transform: 'translateX(0)'})),
                 state('inRight', style({opacity: '1', transform: 'translateX(0)'})),
                     transition('* => inLeft', [
                       animate('500ms ease', keyframes([
                             style({opacity: '0', transform: 'translateX(-100%)', offset: 0}),
                             style({opacity: '1', transform: 'translateX(-100%)', offset: 0.1}),
                             style({opacity: '1', transform: 'translateX(0)', offset: 1}),
                         
                       ]))
                     ]),
                     transition('* => inRight', [
                       animate('500ms ease', keyframes([
                            style({opacity: '0', transform: 'translateX(100%)', offset: 0}),
                            style({opacity: '1', transform: 'translateX(100%)', offset: 0.1}),
                            style({opacity: '1', transform: 'translateX(0)', offset: 1}),
                       ]))
                     ]),
                     transition('* => outRight', [
                       animate('500ms ease', keyframes([
                            style({opacity: '1', transform: 'translateX(0)', offset: 0}),
                            style({opacity: '1', transform: 'translateX(100%)', offset: 0.9}),
                            style({opacity: '0', transform: 'translateX(100%)', offset: 1}),
                       ]))
                     ]),
                     transition('* => outLeft', [
                       animate('500ms ease', keyframes([
                            style({opacity: '1', transform: 'translateX(0)', offset: 0}),
                            style({opacity: '1', transform: 'translateX(-100%)', offset: 0.9}),
                            style({opacity: '0', transform: 'translateX(-100%)', offset: 1}),
                       ]))
                     ])
               ])
             ]
})
export class TabComponent implements OnInit {

  constructor(
          @Optional() @Host() private listTab: ListTabComponent,
          private elementRef: ElementRef
      ) { }
  
  @Input()tittle:any;
  @Input()icon:any;
  state = "init";
  index = 0;
  elementHTML:ElementRef;
  
  ngOnInit() {
      this.elementHTML = this.elementRef;
      if(this.listTab){
          this.listTab.addTab(this);
          this.listTab.setAfterLoad();
      }
      
//      setTimeout(() => {
//          if(this.listTab){
////              this.listTab.getLarguraListAndTabs();
//              this.listTab.setBtnBackNext();
//          }
//      }, 500);
  }
  
  setState(state){
      this.state = state;
  }
  
  setIndex(index){
      this.index = index;
  }
  
  getIndex(){
      return this.index;
  }
  
  getElementRef(){
      return this.elementRef;
  }

}
