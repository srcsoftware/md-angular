import { 
    Component, 
    OnInit, 
    Input, 
    Output, 
    EventEmitter,
    ElementRef,
    Renderer2,
    Optional,
    Host,
    trigger
    ,state
    ,style
    ,transition
    ,animate
    ,keyframes
} from '@angular/core';
import {SelectComponent} from "../../select/select.component"

@Component({
  selector: 'md-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.scss'],
  animations: [
               trigger('zoomOption', [
                 state('in', style({transform: 'scale(1)'})),
                     transition('void => *', [
                       animate('500ms', keyframes([
                         style({opacity: 1, transform: 'scale(0)', offset: 0}),
                         style({opacity: 1, transform: 'scale(1.2)',  offset: 0.7}),
                         style({opacity: 1, transform: 'scale(1)',     offset: 1.0})
                       ]))
                     ]),
                     transition('* => void', [
                       animate('300ms', keyframes([
                         style({opacity: 1,  transform: 'scale(1)',  offset: 0}),
                         style({opacity: 1, offset: 0.4}),
                         style({opacity: 1, transform: 'scale(5)',  offset: 1.0})
                       ]))
                     ])
               ])
             ]
})
export class OptionComponent implements OnInit {
    constructor(
            @Optional() @Host() private select: SelectComponent,
            private elementRef: ElementRef,
            private renderer2: Renderer2
        ) { }
        @Input() addClass = "";
        @Input() width = "12";
        @Input() fontSize = "";
        @Input() id = "id";
        @Input() field = "text";
        @Input() item: Object;
        clickEdit = false;
        clickDelete = false;
        
        ngOnInit() {
            if(this.select){
                let idList = this.select.getId();
                if(idList){
                    this.id = idList;
                }
                let fieldList  = this.select.getField();
                if(fieldList){
                    this.field = fieldList;
                }
                if(this.select.onClick ||this.select.itemSelected || this.select.listSelected){
                    this.renderer2.addClass(this.elementRef.nativeElement, "cursor-hover");
                }
            }
        }
        
        clickOn(item, $event){
            if(!this.clickEdit && !this.clickDelete && (this.select && this.select.onClick ||this.select.itemSelected || this.select.listSelected)){
                this.select.clickOn(item, this.id, $event);
            }
        }
        editOn(item){
            this.clickEdit = true;
            if(this.select && this.select.onEdit){
                this.select.editOn(item)
            }
            setTimeout(() => {
                this.clickEdit = false;
            }, 300);
        }
        deleteOn(item){
            this.clickDelete = true;
            if(this.select && this.select.onDelete){
                this.select.deleteOn(item)
            }
            setTimeout(() => {
                this.clickDelete = false;
            }, 300);
        }
}
