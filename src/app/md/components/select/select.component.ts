import {
    Component, 
    OnInit, 
    Input, 
    Output, 
    EventEmitter,
    ElementRef,
    Renderer2,
    trigger
    ,state
    ,style
    ,transition
    ,animate
    ,keyframes
    
} from '@angular/core';
import {UtilService} from "../../services/util.service";
import {TranslateService} from "../../translate/translate.service";

@Component({
  selector: 'md-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  animations: [
               trigger('zoom', [
                 state('in', style({transform: 'translateX(0) scale(1)', left: "0em"})),
                     transition('void => *', [
                       animate('700ms ease', keyframes([
                         style({opacity: 0, transform: 'scale(0)', offset: 0}),
                         style({opacity: 1, transform: 'scale(1.2)',  offset: 0.7}),
                         style({opacity: 1, transform: 'scale(1)',     offset: 1.0})
                       ]))
                     ]),
                     transition('* => void', [
                       animate('500ms ease-out', keyframes([
                         style({opacity: 1, transform: 'scale(1)',     offset: 0}),
                         style({opacity: 1, transform: 'scale(1.2)', offset: 0.4}),
                         style({opacity: 0, transform: 'scale(0)',  offset: 1.0})
                       ]))
                     ])
               ])
             ]
})
export class SelectComponent implements OnInit {

  constructor(
          private elementRef: ElementRef,
          private renderer2: Renderer2,
          private utilService: UtilService,
          private translate: TranslateService
  ) { }
//  Entradas de dados
  @Input("value") _value:any = "";
  @Input() addClass = "";
  @Input() width = "12";
  @Input() fontSize = "";
  @Input() icon = "";
  @Input() label:any;
  @Input() placeholder = "";
  @Input() msgInput = "";
  @Input() id ="id";
  @Input() field ="text";
  @Input() list = [];
  @Input("listSelected") _listSelected:Object[];
  @Output() listSelectedChange = new EventEmitter();
  @Input("itemSelected") _itemSelected;
  @Output() itemSelectedChange = new EventEmitter();
  @Input() onClick:any;
  @Input() onEdit:any;
  @Input() onDelete:any;
  @Input("filter") sleepList = 500;
  //Variáveis locais
  isFocused = "";
  empty = "";
  triggerList: any;
  listFilter = [];
  isShow = false;
  scrollTopContainer = 0;
  iconClose = "clear";
  _fieldSearch = "";
  iconSearch = "search";
  labelSearch = this.translate.getValue("PESQUISAR");
  elementHtml:any;
  containerSelect:any;
  backSelect:any;
  
  ngOnInit() {
      if(this.label){this.placeholder = ""}
      this.elementHtml = this.elementRef.nativeElement;
      this._fieldSearch = "";
      this.filterList(null);
  }
  
  get value(): any {
    this.setChange();
    return this._value;
  }
  set value(value:any) {
    this._value = value;
    this.setChange();
  }  
  
  set fieldSearch(value:any){
      this._fieldSearch = value;
      this.filterList(value);
  }
  
  setChange(){
    if(this._value){
      this.empty = "";
      
    }else{
      this.empty = "is-empty";
    }
  }
  focusIn($event){
      this.onFocusOnClick($event);
  }
  focusOut(){
      this.isFocused = "";
  }
  onFocusOnClick($event){
      this.isFocused = "is-focused";
      this.openSelect($event);
  }
  
  openSelect($event){
      this.isShow = true;
      this._fieldSearch = "";
      this.filterList(null);
  }
  
  closeSelect(){
      let result = "";
      this.isShow = false;
      if(this.listSelected){
          this._listSelected.forEach(item =>{
              if(result != ""){
                  result += ", "
              }
              result += item[this.field];
          });
      }
      this._value = result;
  }
  
  onShowSelect(){
      this.setPosition();
  }
  
  getId(){
      return this.id;
  }
  getField(){
      return this.field;
  }
  
  get listSelected(){
      return this._listSelected;
  }
  
  set listSelected(value:any){
      this._listSelected = value;
      this.listSelectedChange.emit(value);
  }
    
  get itemSelected(){
      return this._itemSelected;
  }
  set itemSelected(value:any){
      this._itemSelected = value;
      this.itemSelectedChange.emit(value);
  }
  
  clickOn(item, field, $event){
      if(this.itemSelected){
          this.selectSimple(item, field);
      }else if(this.listSelected){
          this.selectMult(item, field);
      }
      if(this.onClick){
          this.onClick(item);
      }
  }
  
  editOn(item){
      this.onEdit(item);
  }
  deleteOn(item){
      this.onDelete(item);
  }
  
  
  selectSimple(item, field){ 
      this.value = item[this.field];
      this.itemSelected.isSelected = false;
      if(!this.itemSelected[field]){
          item.isSelected = true;
          this.itemSelected = item;
      }else if(this.itemSelected[field] == item[field]){
          item.isSelected = false;
          this.itemSelected = {};
          this.value = "";
      }else{
          item.isSelected = true;
          this.itemSelected = item;
      }
      this.closeSelect();
  }
  
  selectMult(item, field){
      let itemSearch = this.utilService.getItemList(this.listSelected, item[field], field);
      if(itemSearch){
          item.isSelected = false;
          this.utilService.removeItemList(this._listSelected, item[field], field);
      }else{
          item.isSelected = true;
          this.listSelected.push(item);
      }
  }
  
  setPosition(){
      let containerSelect = this.elementHtml.querySelector(".md-container-select");
      let posicaoY = this.elementRef.nativeElement.getBoundingClientRect().top;
      let screenYHalf = document.documentElement.clientHeight / 2;
      let heightContainer = parseInt(window.getComputedStyle(containerSelect).height.replace("px", ""));
      let positionContainer = heightContainer * -0.38;
      if(this.isShow){//Caso seja o OPEN
          if((heightContainer * 0.75) > posicaoY || posicaoY < screenYHalf){
              if((posicaoY - (heightContainer * 0.38)) < 0){
                  positionContainer = heightContainer * -0.04;
              }
          }else if((posicaoY + (heightContainer * 0.64)) > (screenYHalf * 2)){
              positionContainer = heightContainer * -0.85;
              
          }
          this.renderer2.setStyle(containerSelect, "top", positionContainer + "px");
          if(this.scrollTopContainer){
              containerSelect.scrollTop = this.scrollTopContainer;
          }
          this.renderer2.setStyle(containerSelect, "top", positionContainer + "px");
      }else{
          this.scrollTopContainer = containerSelect.scrollTop;
      }
  }
  
  filterList(value){
      if(this.triggerList){
          clearInterval(this.triggerList);
      }
      this.triggerList = setTimeout(() => {
          if(value){
              this.listFilter = this.utilService.filterItemList(this.list, value, this.field);
          }else{
              this.listFilter = this.list;
          }
      }, this.sleepList);
  }
  
  onScrollOption($event){
      let containerOption = $event.target;
      let containerHover = containerOption.querySelector(".md-container-option-hover");
      let classHover = "shadow-bottom-hover";
      if(containerOption.scrollTop > 0 && !containerOption.querySelector("." + classHover)){
          
          this.renderer2.addClass(containerHover, classHover);
      }else if(containerOption.querySelector("." + classHover) && containerOption.scrollTop <= 0){
          this.renderer2.removeClass(containerHover, classHover);
      }
  }
}
