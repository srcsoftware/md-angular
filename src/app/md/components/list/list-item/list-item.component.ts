import { Component, OnInit, Input, Host, Optional, ElementRef, Renderer2} from '@angular/core';
import {ListComponent} from "../list.component";

@Component({
  selector: 'md-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  constructor(
      @Optional() @Host() private list: ListComponent,
      private elementRef: ElementRef,
      private renderer2: Renderer2
  ) { }
  @Input() addClass = "";
  @Input() width = "12";
  @Input() fontSize = "";
  @Input() id = "id";
  @Input() field = "text";
  @Input() item: Object;
  clickEdit = false;
  clickDelete = false;
  
  ngOnInit() {
      if(this.list){
          let idList = this.list.getId();
          if(idList){
              this.id = idList;
          }
          let fieldList  = this.list.getField();
          if(fieldList){
              this.field = fieldList;
          }
          if(this.list.onClick ||this.list.itemSelected || this.list.listSelected){
              this.renderer2.addClass(this.elementRef.nativeElement, "cursor-hover");
          }
      }
  }
  
  clickOn(item){
      if(!this.clickEdit && !this.clickDelete && (this.list && this.list.onClick ||this.list.itemSelected || this.list.listSelected)){
          this.list.clickOn(item, this.id);
      }
  }
  editOn(item){
      this.clickEdit = true;
      if(this.list && this.list.onEdit){
          this.list.editOn(item)
      }
      setTimeout(() => {
          this.clickEdit = false;
      }, 300);
  }
  deleteOn(item){
      this.clickDelete = true;
      if(this.list && this.list.onDelete){
          this.list.deleteOn(item)
      }
      setTimeout(() => {
          this.clickDelete = false;
      }, 300);
  }

}
