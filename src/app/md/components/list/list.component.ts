import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {UtilService} from "../../services/util.service";

@Component({
  selector: 'md-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(
          private utilService: UtilService
  ) { }
  
  @Input() addClass = "";
  @Input() width = "12";
  @Input() fontSize = "";
  @Input() id = "id";
  @Input() field = "text";
  @Input() list = [];
  @Input("listSelected") _listSelected;
  @Output() listSelectedChange = new EventEmitter();
  @Input("itemSelected") _itemSelected;
  @Output() itemSelectedChange = new EventEmitter();
  @Input() onClick:any;
  @Input() onEdit:any;
  @Input() onDelete:any;

  ngOnInit() {
      console.log("Lista Inicio");
  }
  
  getId(){
      return this.id;
  }
  getField(){
      return this.field;
  }
  
  get listSelected(){
      return this._listSelected;
  }
  
  set listSelected(value:any){
      this._listSelected = value;
      this.listSelectedChange.emit(this._listSelected);
  }
    
  get itemSelected(){
      return this._itemSelected;
  }
  set itemSelected(value:any){
      this._itemSelected = value;
      console.log(value);
      this.itemSelectedChange.emit(this._itemSelected);
  }
  
  clickOn(item, field){
      if(this.itemSelected){
          this.selectSimple(item, field);
      }else if(this.listSelected){
          this.selectMult(item, field);
      }
      if(this.onClick){
          this.onClick(item);
      }
  }
  
  editOn(item){
      this.onEdit(item);
  }
  deleteOn(item){
      this.onDelete(item);
  }
  
  
  selectSimple(item, field){      
      if(!this.itemSelected[field]){
          item.isSelected = true;
          this.itemSelected = item;
      }else if(this.itemSelected[field] == item[field]){
          item.isSelected = false;
          this.itemSelected = {};
      }else{
          this.itemSelected.isSelected = false;
          item.isSelected = true;
          this.itemSelected = item;
      }
  }
  
  selectMult(item, field){
      let itemSearch = this.utilService.getItemList(this.listSelected, item[field], field);
      if(itemSearch){
          item.isSelected = false;
          this.utilService.removeItemList(this._listSelected, item[field], field);
      }else{
          item.isSelected = true;
          this.listSelected.push(item);
      }
  }
  
}
