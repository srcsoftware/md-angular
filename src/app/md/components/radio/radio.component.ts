import { Component, OnInit, Input, Output, EventEmitter, ElementRef, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'md-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {

  constructor(
          private elementRef: ElementRef,
          private renderer2: Renderer2
   ) { }
  isChecked = false;
  @Input("value") _value:any = false;
  @Output() valueChange = new EventEmitter();
  @ViewChild("inputRadio") inputRadio: ElementRef;
  @Input() addClass = "";
  @Input() width = "";
  @Input() fontSize = "";
  @Input() label = "-";
  @Input() disabled = false;
  @Input() msgInput = "";
  @Input() name:any;
  @Input() setValue:any;
  @Input() tooltip:any;
  
  
  get value(): any {
    return this._value;
  }
  
  set value(value:any) {
    this._value = value;
    this.valueChange.emit(this._value);
  }  
  
  validChecks(){
      if(this.value == this.setValue){
          this.isChecked = true;
      }else{
          this.isChecked = false;
      }
      console.log(this.isChecked);
  }
  
  check(){
      if(!this.disabled){
          this.renderer2.setProperty(this.inputRadio.nativeElement, "checked", true);
          this.value = this.setValue;
      }
  }
  ngOnInit() {
     
  }
}
