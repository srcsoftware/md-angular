import { Component, OnInit, Input, Output, EventEmitter, ElementRef, Renderer2} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'md-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextAreaComponent implements OnInit {
  constructor(
              private element: ElementRef,
              private render: Renderer2
          ) { 
  }
  empty = "";
  isFocused = "";
  @Input("value") _value:any = "";
  @Output() valueChange = new EventEmitter();
  @Input() addClass = "";
  @Input() width = "12";
  @Input() fontSize = "";
  @Input() iconBefore = "";
  @Input() iconAfter = "";
  @Input() label = "";
  @Input() placeholder = "";
  @Input() msgInput = "";
  @Input() rows = "";
  @Input() cols = "";
  
  
  get value(): any {
    this.setChange();
    return this._value;
  }
  set value(value:any) {
    this._value = value;
    this.valueChange.emit(this._value);
    this.setChange();
  }  

  ngOnInit() {
      if(this.label){this.placeholder = ""}
      if(!this.rows){this.rows = "1"}
  }
  
  setChange(){
    if(this._value){
      this.empty = "";
      
    }else{
      this.empty = "is-empty";
    }
  }
  focusIn(){
      this.isFocused = "is-focused";
  }
  focusOut(){
      this.isFocused = "";
  }
  
  
}
