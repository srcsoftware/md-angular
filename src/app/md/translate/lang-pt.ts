export const LANG_PT_NAME = 'pt';

export const LANG_PT_TRANS = {
    "BEM_VINDOstatus": "Bem vindos",
    "PESQUISAR": "Pesquisar",
//------------------------------------------------------------------------    Erros de stastus do servidor
    "statusDefault": "Erro ao tentar conectar com servidor.",
    "status302": "O recurso foi movido temporariamente para outra URI.",
    "status304": "O recurso não foi alterado.",
    "status400": "Os dados da requisição não são válidos.",
    "status401": "A URI especificada exige autenticação do cliente. O cliente pode tentar fazer novas requisições.",
    "status403": "O servidor entende a requisição, mas se recusa em atendê-la. O cliente não deve tentar fazer uma nova requisição.",
    "status404": "O servidor não encontrou nenhuma URI correspondente.",
    "status405": "O método especificado na requisição não é válido na URI. A resposta deve incluir um cabeçalho Allow com uma lista dos métodos aceitos.",
    "status410": "O recurso solicitado está indisponível mas seu endereço atual não é conhecido.",
    "status500": "O servidor não foi capaz de concluir a requisição devido a um erro inesperado.",
    "status502": "O servidor, enquanto agindo como proxy ou gateway, recebeu uma resposta inválida do servidor upstream a que fez uma requisição.",
    "status503": "O servidor, enquanto agindo como proxy ou gateway, recebeu uma resposta inválida do servidor upstream a que fez uma requisição.",
};