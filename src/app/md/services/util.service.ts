import { Injectable } from '@angular/core';
import {TranslateService} from "../translate/translate.service"

@Injectable()
export class UtilService {

  constructor(private translateService: TranslateService) { }
  
  getOnlyNumbers(value){
      if(value){
          try {
              return value.replace(/[^0-9]+/g, "");
          } catch (e) {console.log(e);}
      }
  };
  
  getMsgStatusError(status){
      var msg = this.translateService.getValue("statusDefault");
      switch (status) {
          case 302: msg = this.translateService.getValue("status302"); break;
          case 304: msg = this.translateService.getValue("status304"); break;
          case 400: msg = this.translateService.getValue("status400"); break;
          case 401: msg = this.translateService.getValue("status401"); break;
          case 403: msg = this.translateService.getValue("status403"); break;
          case 404: msg = this.translateService.getValue("status404"); break;
          case 405: msg = this.translateService.getValue("status405"); break;
          case 410: msg = this.translateService.getValue("status410"); break;
          case 500: msg = this.translateService.getValue("status500"); break;
          case 502: msg = this.translateService.getValue("status302"); break;
          case 503: msg = this.translateService.getValue("status302"); break;
          default: break;
      }
      return msg;
      
  };
  
  getMsgResult = function(result){
      var msg = null;
      var tipo = "";
      if(result.msgError){tipo = "erro";msg = result.msgError;}
      else if(result.msgInformacao){tipo = "informacao";msg = result.msgInformacao;}
      else if(result.msgSucesso){tipo = "sucesso";msg = result.msgSucesso;}
      else if(result.msgAtencao){tipo = "atencao";msg = result.msgAtencao;}
      //if(msg){utilDialog.closeLoading();utilDialog.showMsg(msg, null, tipo);}
  };
  
  
  getItemList = function(lista, value, campo){
      if(!campo){campo = "id"};
      var item = null;
      if(value && lista){
          for(var index = 0; index < lista.length; index++){
              if(lista[index][campo] == value){
                  item = lista[index];
                  break;
              }
          }
      }
      return item;
  };
  
  removeItemList = function(lista, value, campo){
      if(!campo){campo = "id"};      
      var item = null;
      if(value && lista){
          for(var index = 0; index < lista.length; index++){
              if(lista[index][campo] == value){
                  lista.splice(index, 1);
                  break;
              }
          }
      }
  };
  
  filterItemList = function(lista, value:string, campo:string){
      if(!campo){campo = "id"};
      if(!value) return lista;
      return lista.filter(item => {
          return  item[campo].toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1;
      });
  };
  
  
  
//  
//  utilFunc.setPositionSelect = function(element){
//      var heightfiltro = parseInt(element.find(".lista-select").css("height").replace("px", ""));
//      var height = parseInt(element.find(".resultado-lista-select").css("height").replace("px", ""));
//      var w = angular.element($window);
//      var offSet = element.offset().top;
//      var scrollTop = w.scrollTop();
//      var height = element.find(".lista-select").css("height").replace("px", "");
//      var heightInput = parseInt(element.find(".form-control:first").css("height").replace("px", ""));
//      var center = (w.height()/2);
//      var valor =  (heightInput -  (heightInput/2)) * -1;
//      if(((offSet + heightInput) - scrollTop) >= center && offSet > height){
//              valor = ((heightfiltro - height)/3) * -1;
//              element.find(".lista-select").css("top", "auto");
//              element.find(".lista-select").css("bottom", valor);
//      }else{
//              element.find(".lista-select").css("bottom", "auto");
//              element.find(".lista-select").css("top", valor);
//      }
//  };
//  
//  utilFunc.setTableFixed = function(element){
//      angular.element($window).bind("scroll", function() {
//          var topTable = element.offset().top;
//          var widthTable = element.find("#firstHead:first").width();
//          var leftTable = element.find("#firstHead:first").offset().left;
//          var tableFixed = element.find(".table-fixed");
//          var top = 0;
//          if (this.pageYOffset >= topTable) {
//              if($document.find(".nav-fixed").html()){
//                  top = $document.find(".nav-fixed").height();
//              }
//              tableFixed.css({"min-width": "100%", "top": top, "left": 0, "right": 0});
//              tableFixed.find(".table").css({"width": widthTable, "margin-left": leftTable, "margin-bottom": 0});
//              tableFixed.fadeIn(300);
//          } else {
//              tableFixed.fadeOut(300);
//          }
//      });
//  };
//  
//  utilFunc.configLoading = function(scope, element) {
//      $timeout(function() {
//          var lgText = 0;
//          if (scope.label) {
//              lgText = parseInt(element.find(".spinner-label").css("width"));
//          }
//          var lgSpinner = parseInt(element.find(".spinner").css("width"));
//          var lgContainer = parseInt(element.css("width"));
//          var position = (lgContainer / 2) - ((lgText + lgSpinner) / 2);
//          position = (position * 100) / lgContainer;
//          element.find(".container-spinner").css({
//              left : Math.round(position) + "%",
//              opacity : 1,
//              width : (90 - position) + "%"
//          });
//      }, 200);
//  };
//  
//  utilFunc.loadPage = function(page, scope){
//      var $message = $('<div></div>');
//   $message.load(page,null, function(){
//      $message = $compile($message)(scope);
//   });
//   return $message;
//  };
//  
//  utilFunc.setFixeNav = function(element, isMin){
//      $timeout(function(){
//          var heightNav = element.find(".navbar").height();
//          var navFaker = element.find(".nav-faker");
//          var navFixed = element.find(".nav-not-faker");
//          navFixed.addClass("nav-fixed");
//          if(isMin){
//                  navFaker.addClass("nav-min");
//                  navFixed.addClass("nav-min");
//          }else{
//                  navFaker.removeClass("nav-min");
//                  navFixed.removeClass("nav-min");
//          }
//          $timeout(function(){
//                  heightNav = element.find(".navbar").height();
//                  console.log("height: " + heightNav);
//                  navFaker.css({height: (heightNav *1.47), display: "block"});
//          },300);
//      },300);
//  };

}
